<?php

use Illuminate\Database\Seeder;

class ContactSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('contacts')->insert([
            'name' => 'Stephen Mike',
            'tel' => '07675334545',
        ]);

        DB::table('contacts')->insert([
            'name' => 'Stephen Jones',
            'tel' => '06556543545',
        ]);

        DB::table('contacts')->insert([
            'name' => 'Anna Jones',
            'tel' => '07564354545',
        ]);
    }
}
