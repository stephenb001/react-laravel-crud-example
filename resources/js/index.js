import React from 'react';
import ReactDOM from 'react-dom';
// Below add ability to allow React to route.
import {BrowserRouter, Route, Switch } from 'react-router-dom';
import Home from "./components/Home";
import Add from "./components/Add";
import Edit from "./components/Edit";
import Delete from "./components/Delete";


function Index() {
    return (
        <BrowserRouter>
            <Switch>
                <Route path='/' exact component={Home}/>
                <Route path='/add' exact component={Add}/>
                <Route path='/:id/edit' exact component={Edit}/>
                <Route path='/:id/delete' exact component={Delete}/>
            </Switch>
        </BrowserRouter>
    );
}

export default Index;

if (document.getElementById('app')) {
    ReactDOM.render(<Index />, document.getElementById('app'));
}
