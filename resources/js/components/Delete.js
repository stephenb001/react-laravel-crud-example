import React, { Component } from 'react';
import ReactDOM from 'react-dom';

class Delete extends Component {

    constructor(props) {
        super(props);
        this.state = {
            name: '',
            tel: ''
        }
    }

    componentDidMount() {
        const id = this.props.match.params.id;
        axios.get(`/api/contact/${id}/edit`)
            .then(response => {
                this.setState({
                    name: response.data.name,
                    tel: response.data.tel
                });
            }).catch(err => {console.log(err)});
    }

    handleFormSubmit = event => {

        event.preventDefault();
        const id = this.props.match.params.id;
        axios.delete(`/api/contact/${id}/delete`, {
            name: this.state.name,
            tel: this.state.tel
        }).then(response => {
            this.setState({
                name: '',
                tel: ''
            });
            this.props.history.push('/');
        }).catch(err => {
            console.log(err);
        })
    }

    handleFormCancel = event => {
        this.props.history.push('/');
    }

    render() {
        return (
            <div className="container">
                <div className="row justify-content-center">
                    <div className="col-md-8">
                        <div className="card">
                            <div className="card-header">Delete Contact</div>

                            <div className="card-body">
                                <form onSubmit={this.handleFormSubmit}>
                                    <div className="form-group">
                                        <label htmlFor="exampleInputEmail1">Name: {this.state.name}</label>
                                    </div>
                                    <div className="form-group">
                                        <label htmlFor="exampleInputPassword1">Tel: {this.state.tel}</label>
                                    </div>
                                    <button type="submit" className="btn btn-outline-danger mr-5" name="confirm">Delete</button>
                                    <button type="button" onClick={this.handleFormCancel} className="btn btn-primary" name="cancel">Cancel</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default Delete;
