import React, { Component } from 'react';
import ReactDOM from 'react-dom';

class Edit extends Component {

    constructor(props) {
        super(props);
        this.state = {
            name: '',
            tel: ''
        }
    }

    componentDidMount() {
        const id = this.props.match.params.id;
        axios.get(`/api/contact/${id}/edit`)
            .then(response => {
                this.setState({
                    name: response.data.name,
                    tel: response.data.tel
                });
            }).catch(err => {console.log(err)});
    }

    handleNameInputChange = event => {
        this.setState({
            name: event.target.value
        })
    }

    handleTelInputChange = event => {
        this.setState({
            tel: event.target.value
        })
    }

    handleFormSubmit = event => {
        event.preventDefault();
        const id = this.props.match.params.id;
        axios.put(`/api/contact/${id}/update`, {
            name: this.state.name,
            tel: this.state.tel
        }).then(response => {
            this.setState({
                name: '',
                tel: ''
            });
            this.props.history.push('/');
        }).catch(err => {
            console.log(err);
        })
    }

    render() {
        return (
            <div className="container">
                <div className="row justify-content-center">
                    <div className="col-md-8">
                        <div className="card">
                            <div className="card-header">Edit Contact</div>

                            <div className="card-body">
                                <form onSubmit={this.handleFormSubmit}>
                                    <div className="form-group">
                                        <label htmlFor="exampleInputEmail1">Name</label>
                                        <input
                                            type="text"
                                            className="form-control"
                                            placeholder="Name"
                                            required
                                            onChange={this.handleNameInputChange}
                                            value={this.state.name}
                                        />
                                    </div>
                                    <div className="form-group">
                                        <label htmlFor="exampleInputPassword1">Tel</label>
                                        <input
                                            type="text"
                                            className="form-control"
                                            placeholder="Telephone"
                                            required
                                            onChange={this.handleTelInputChange}
                                            value={this.state.tel}
                                        />
                                    </div>
                                    <button type="submit" className="btn btn-primary">Submit</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default Edit;
